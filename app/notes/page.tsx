'use client';

import { useState, useMemo } from 'react';
import { Stack, HStack, Box, Text, Container, Center } from '@chakra-ui/react';
import { useAppSelector } from '@/hooks';
import { ColumnHeader, ColumnHeading, ColumnButton } from '@/components/column';
import { FiFilter } from 'react-icons/fi';
import { type NoteMeta } from '@/data/features/notes/notes-slice';
import { Note } from '@components/note';

function DefaultNote() {
  return (
    <Center>
      <Text>Please select a note</Text>
    </Center>
  );
}

// TODO: add app topbar with justification and note styling
export default function Page() {
  const [noteMeta, setNoteMeta] = useState<NoteMeta | null>(null);

  const [sidebarIsScrolled, setSidebarIsScrolled] = useState(false);
  const [mainIsScrolled, setMainIsScrolled] = useState(false);

  const notes = useAppSelector((state) => state.notes.graphData.nodes);
  const sortedNotes = useMemo(() => {
    const _notes = notes?.slice();
    return _notes?.sort((a, b) => a.title.localeCompare(b.title));
  }, [notes]);

  return (
    <>
      <Box
        borderRightWidth="1px"
        width={{ md: '20rem', xl: '24rem' }}
        display={{ base: 'none', md: 'initial' }}
        overflowY="auto"
        onScroll={(event) =>
          setSidebarIsScrolled(event.currentTarget.scrollTop > 32)
        }
      >
        <ColumnHeader shadow={sidebarIsScrolled ? 'base' : 'none'}>
          <HStack justify="space-between" width="full">
            <HStack spacing="3">
              <ColumnHeading>Notes</ColumnHeading>
            </HStack>
            <ColumnButton leftIcon={<FiFilter />}>Filter</ColumnButton>
          </HStack>
        </ColumnHeader>
        <Stack spacing={{ base: '1px', lg: '1' }} px={{ lg: '3' }} py="3">
          {notes ? (
            sortedNotes.map((note) => (
              <Box
                key={note.id}
                borderRadius={{ lg: 'lg' }}
                onClick={() => setNoteMeta(note)}
                _hover={{ textDecoration: 'none', bg: 'gray.100' }}
                _activeLink={{ bg: 'gray.600', color: 'white' }}
                aria-current={noteMeta?.id === note.id ? 'page' : undefined}
              >
                <Stack
                  spacing="1"
                  py={{ base: '3', lg: '2' }}
                  px={{ base: '3.5', lg: '3' }}
                  fontSize="sm"
                  lineHeight="1.25rem"
                >
                  <Text fontWeight="medium">{note.title}</Text>
                </Stack>
              </Box>
            ))
          ) : (
            <Text fontWeight="medium">No Notes Found!</Text>
          )}
        </Stack>
      </Box>
      <Container as="main" pt="8">
        <Center>{noteMeta ? <Note note={noteMeta} /> : <DefaultNote />}</Center>
      </Container>
    </>
  );
}
