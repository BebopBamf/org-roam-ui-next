'use client';

import { Box, Container, Text, Stack, Center } from '@chakra-ui/react';
import { Link } from '@chakra-ui/next-js';

export default function Home() {
  return (
    <Box flex="1">
      <Container as="main" mt="8">
        <Center>
          <Stack spacing="4">
            <Link href="/graph">Navigate to the Force Graph</Link>
            <Link href="/notes">Navigate to Notes</Link>
            <Link href="/random">Navigate to a Random Note</Link>
            <Text>Read dail(y|ies)</Text>
          </Stack>
        </Center>
      </Container>
    </Box>
  );
}
