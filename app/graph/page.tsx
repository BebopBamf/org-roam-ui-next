'use client';

import { Container, Text, Stack, Center } from '@chakra-ui/react';

export default function Graph() {
  return (
    <Container as="main" pt="8">
      <Center>
        <Stack spacing="4">
          <Text>Navigate to the Force Graph</Text>
          <Text>Navigate to the Notes</Text>
          <Text>Navigate to a Random Note</Text>
          <Text>Read dail(y|ies)</Text>
        </Stack>
      </Center>
    </Container>
  );
}
