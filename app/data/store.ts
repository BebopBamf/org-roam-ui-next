import { configureStore } from '@reduxjs/toolkit';
import { apiSlice } from './features/api/api-slice';
import colorSchemeReducer from './features/color-scheme/color-scheme-slice';
import apiStatusReducer from './features/api-status/api-status-slice';
import notesReducer from './features/notes/notes-slice';
import emacsVariablesReducer from './features/emacs-variables/emacs-variables-slice';

export const makeStore = () => {
  return configureStore({
    reducer: {
      notes: notesReducer,
      emacsVariables: emacsVariablesReducer,
      colorScheme: colorSchemeReducer,
      apiStatusReducer: apiStatusReducer,
      [apiSlice.reducerPath]: apiSlice.reducer,
    },
    middleware: (getDefaultMiddleware) =>
      getDefaultMiddleware().concat(apiSlice.middleware),
  });
};

// Infer the type of makeStore
export type AppStore = ReturnType<typeof makeStore>;

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<AppStore['getState']>;
export type AppDispatch = AppStore['dispatch'];
