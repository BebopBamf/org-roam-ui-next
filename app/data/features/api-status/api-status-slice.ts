import { createSlice } from '@reduxjs/toolkit';

export const apiStatusSlice = createSlice({
  name: 'apiStatus',
  initialState: {
    connected: false,
  },
  reducers: {
    connect: (state) => {
      state.connected = true;
    },
    disconnect: (state) => {
      state.connected = false;
    },
  },
});

export const { connect, disconnect } = apiStatusSlice.actions;

export default apiStatusSlice.reducer;
