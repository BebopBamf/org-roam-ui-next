import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

export const apiSlice = createApi({
  reducerPath: 'api',
  baseQuery: fetchBaseQuery({
    baseUrl: 'http://localhost:35901',
    responseHandler: 'text',
  }),
  endpoints: (builder) => ({
    getNote: builder.query<string, string>({
      query: (id) => `node/${id}`,
    }),
  }),
});

export const { useGetNoteQuery } = apiSlice;
