import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export type EmacsVariables = {
  roamDir: string;
  attachDir: string;
  dailyDir: string;
  subDirs: string[];
  katexMacros: { [key: string]: string }[];
  useInheritance: boolean;
};

const initialState: EmacsVariables = {
  roamDir: '',
  attachDir: '',
  dailyDir: '',
  subDirs: [],
  katexMacros: [],
  useInheritance: false,
};

export const emacsVariablesSlice = createSlice({
  name: 'emacsVariables',
  initialState,
  reducers: {
    setEmacsVariables: (state, action: PayloadAction<EmacsVariables>) => {
      state.roamDir = action.payload.roamDir;
      state.attachDir = action.payload.attachDir;
      state.dailyDir = action.payload.dailyDir;
      state.subDirs = action.payload.subDirs;
      state.katexMacros = action.payload.katexMacros;
      state.useInheritance = action.payload.useInheritance;
    },
  },
});

export const { setEmacsVariables } = emacsVariablesSlice.actions;

export default emacsVariablesSlice.reducer;
