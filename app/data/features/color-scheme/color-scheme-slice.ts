import { createSlice, type PayloadAction } from '@reduxjs/toolkit';
import { colorSchemes } from './color-schemes';

export const defaultColorScheme = 'gruvbox';
export const defaultHighlightColor = 'purple.800';

export const colorSchemeOptions = Object.keys(colorSchemes);

export type ColorScheme = [string, { [key: string]: string }];

export const getColorScheme = (colorScheme: string): ColorScheme => [
  colorScheme,
  colorSchemes[colorScheme],
];

interface ColorSchemeState {
  colorScheme: ColorScheme;
  highlightColor: string;
}

const initialState: ColorSchemeState = {
  colorScheme: getColorScheme(defaultColorScheme),
  highlightColor: defaultHighlightColor,
};

export const colorSchemeSlice = createSlice({
  name: 'colorScheme',
  initialState,
  reducers: {
    setFromLocalStorage: (state) => {
      const initialColorScheme = getColorScheme(defaultColorScheme);
      state.colorScheme =
        JSON.parse(
          localStorage.getItem('colorScheme') ??
            JSON.stringify(initialColorScheme)
        ) ?? initialColorScheme;

      state.highlightColor =
        JSON.parse(
          localStorage.getItem('highlightColor') ??
            JSON.stringify(defaultHighlightColor)
        ) ?? defaultHighlightColor;
    },
    setColorScheme: (state, action: PayloadAction<ColorScheme>) => {
      state.colorScheme = action.payload;
    },
    setHighlightColor: (state, action: PayloadAction<string>) => {
      state.highlightColor = action.payload;
    },
  },
});

export const { setColorScheme, setHighlightColor, setFromLocalStorage } =
  colorSchemeSlice.actions;

export default colorSchemeSlice.reducer;
