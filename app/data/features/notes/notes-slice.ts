import { createSlice, type PayloadAction } from '@reduxjs/toolkit';

export enum LinkType {
  Cite = 'cite',
  Ref = 'ref',
  Id = 'id',
}

export type Link = {
  type: LinkType;
  source: string;
  target: string;
};

export type NoteMeta = {
  id: string;
  title: string;
  file: string;
  tags: string[];
  properties: { [key: string]: string };
  olp: null;
  pos: number;
  level: number;
};

export type Note = {
  id: string;
  content: string;
};

export type GraphData = {
  links: Link[];
  nodes: NoteMeta[];
  tags: string[];
};

export type NoteState = {
  notes: Note[];
  graphData: GraphData;
};

const initialState: NoteState = {
  notes: [],
  graphData: {
    links: [],
    nodes: [],
    tags: [],
  },
};

export const notesSlice = createSlice({
  name: 'notesSlice',
  initialState,
  reducers: {
    setNotes: (state, action: PayloadAction<GraphData>) => {
      state.graphData = action.payload;
    },
  },
});

export const { setNotes } = notesSlice.actions;

export default notesSlice.reducer;
