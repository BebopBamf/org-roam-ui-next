import { StyleFunctionProps } from '@chakra-ui/react';
import { interpolate } from 'd3-interpolate';

type ColorScheme = {
  [key: string]: string;
};

export const getBorderColor = (theme: ColorScheme, color: string) => {
  if (color === 'purple.500') {
    return `${theme['violet']}aa`;
  }
  if (color === 'pink.500') {
    return `${theme['magenta']}aa`;
  }
  if (color === 'blue.500') {
    return `${theme['blue']}aa`;
  }
  if (color === 'cyan.500') {
    return `${theme['cyan']}aa`;
  }
  if (color === 'green.500') {
    return `${theme['green']}aa`;
  }
  if (color === 'yellow.500') {
    return `${theme['yellow']}aa`;
  }
  if (color === 'orange.500') {
    return `${theme['orange']}aa`;
  }
  if (color === 'red.500') {
    return `${theme['red']}aa`;
  }
};

export const makeTheme = (themeColors: ColorScheme, highlightColor: string) => {
  const borderColor = getBorderColor(themeColors, highlightColor);
  const missingColor = interpolate(
    themeColors['base1'],
    themeColors['base2']
  )(0.2);

  return {
    colors: {
      white: themeColors['bg'],
      black: themeColors['fg'],
      gray: {
        100: themeColors['base1'],
        200: missingColor,
        300: themeColors['base2'],
        400: themeColors['base3'],
        500: themeColors['base4'],
        600: themeColors['base5'],
        700: themeColors['base6'],
        800: themeColors['base7'],
        900: themeColors['base8'],
      },
      blue: {
        500: themeColors['blue'],
      },
      teal: {
        500: themeColors['blue'],
      },
      yellow: {
        500: themeColors['yellow'],
      },
      orange: {
        500: themeColors['orange'],
      },
      red: {
        500: themeColors['red'],
      },
      green: {
        500: themeColors['green'],
      },
      purple: {
        500: themeColors['violet'],
      },
      pink: {
        500: themeColors['magenta'],
      },
      cyan: {
        500: themeColors['cyan'],
      },
      alt: {
        100: themeColors['bg-alt'],
        900: themeColors['fg-alt'],
      },
    },
    shadows: {
      outline: '0 0 0 3px ' + borderColor,
    },
    components: {
      Button: {
        variants: {
          outline: {
            border: '2px solid',
            borderColor: highlightColor,
            color: highlightColor,
          },
          ghost: {
            color: highlightColor,
            _hover: {
              bg: `inherit`,
              border: '1px solid',
              borderColor: highlightColor,
            },
            _active: { color: `inherit`, bg: highlightColor },
          },
          subtle: {
            color: 'gray.800',
            _hover: { bg: `inherit`, color: highlightColor },
            _active: { color: `inherit`, bg: borderColor },
          },
        },
      },
      Accordion: {
        baseStyle: {
          container: {
            marginTop: '10px',
            borderWidth: '0px',
            _last: {
              borderWidth: '0px',
            },
          },
          panel: {
            marginRight: '10px',
          },
        },
      },
      Slider: {
        baseStyle: (props: StyleFunctionProps) => ({
          thumb: {
            backgroundColor: highlightColor,
          },
          filledTrack: {
            backgroundColor: 'gray.400',
          },
          track: {
            backgroundColor: 'gray.400',
            borderColor: 'gray.400',
            borderWidth: '5px',
            borderRadius: 'lg',
          },
        }),
      },
    },
  };
};
