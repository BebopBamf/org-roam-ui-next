import type { Metadata } from 'next';

import { Providers } from '@components/layout/providers';
import { AppLayout } from '@components/layout/app-layout';

export const metadata: Metadata = {
  title: 'Org Roam UI',
  description: 'Generated by create next app',
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en" suppressHydrationWarning>
      <body>
        <Providers>
          <AppLayout>{children}</AppLayout>
        </Providers>
      </body>
    </html>
  );
}
