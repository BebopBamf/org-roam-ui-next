'use client';

import { type ReactNode } from 'react';
import { Text } from '@chakra-ui/react';

export type LinkProps = {
  href: string;
  children: ReactNode;
};

export function PreviewLink(props: LinkProps) {
  const { href, children } = props;

  return (
    <Text
      as="span"
      display="inline"
      className={href}
      color="base.700"
      cursor="not-allowed"
    >
      {children}
    </Text>
  );
}
