'use client';

import React, { useMemo } from 'react';
import { Processor, unified } from 'unified';
import uniorgParse from 'uniorg-parse';
import extractKeywords from 'uniorg-extract-keywords';
import uniorgRehype from 'uniorg-rehype';
import uniorgSlug from 'uniorg-slug';
import attachments from 'uniorg-attach';
import katex from 'rehype-katex';
import rehypeReact from 'rehype-react';
import * as prod from 'react/jsx-runtime';

import { PreviewLink } from './link';
import { type NoteMeta } from '@data/features/notes/notes-slice';

export type OrgViewProps = {
  meta: NoteMeta;
  content: string;
  attachDir: string;
  useInheritance: boolean;
};

export function OrgView(props: OrgViewProps) {
  const { meta, attachDir, useInheritance } = props;

  const uniorgProcessor = unified()
    .use(uniorgParse)
    .use(extractKeywords)
    .use(attachments, {
      idDir: attachDir,
      useInheritance,
    })
    .use(uniorgSlug)
    .use(uniorgRehype, { useSections: true });

  // TODO: add markdown processor
  const baseProcessor = uniorgProcessor as Processor<any, any, any, any, any>;

  const processor = useMemo(() => {
    return baseProcessor
      .use(katex, {
        trust: (context) => ['\\htmlId', '\\href'].includes(context.command),
        macros: {
          '\\eqref': '\\href{###1}{(\\text{#1})}',
          '\\ref': '\\href{###1}{\\text{#1}}',
          '\\label': '\\htmlId{#1}{}',
        },
      })
      .use(rehypeReact, {
        Fragment: prod.Fragment,
        jsx: prod.jsx,
        jsxs: prod.jsxs,
        components: {
          a: ({ children, href }) => {
            return (
              <PreviewLink href={`${href as string}`}>{children}</PreviewLink>
            );
          },
          img: ({ src, alt }) => {
            return <img src={src as string} alt={alt as string} />;
          },
          section: ({ children }) => {
            return <>{children}</>;
          },
          blockquote: ({ children }) => {
            return <blockquote>{children}</blockquote>;
          },
          p: ({ children }) => {
            return <p lang="en">{children}</p>;
          },
        },
      });
  }, [meta]);

  const Content = useMemo(() => {
    console.log('Content: ', props.content);
    const result = processor.processSync(props.content).result;
    console.log('Result: ', result);
    return result;
  }, [props.content]);

  return Content as React.ReactElement;
}
