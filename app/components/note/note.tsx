import { Box } from '@chakra-ui/react';
import { type NoteMeta } from '@data/features/notes/notes-slice';
import { useGetNoteQuery } from '@data/features/api/api-slice';
import { OrgView } from './org-view';

export type NoteProps = {
  note: NoteMeta;
  justification: number;
  justificationList: string[];
  macros?: { [key: string]: string };
  attachDir: string;
  useInheritance: boolean;
};

function Loading() {
  return <Box>Loading...</Box>;
}

function Error() {
  return <Box>Error</Box>;
}

export function Note(props: NoteProps) {
  const { note } = props;
  const { data, isLoading, isError } = useGetNoteQuery(note.id);

  if (isLoading) {
    return <Loading />;
  }

  if (isError || !data) {
    return <Error />;
  }

  return (
    <Box
      pr={8}
      pt={2}
      height="100%"
      className="org"
      sx={{
        textAlign: justificationList[justification],
      }}
    >
      <OrgView meta={note} content={data} attachDir="/" useInheritance={true} />
    </Box>
  );
}
