import {
  Flex,
  Heading,
  IconButton,
  Button,
  type FlexProps,
  type HeadingProps,
  type ButtonProps,
  type IconButtonProps,
} from '@chakra-ui/react';

export const ColumnHeader = (props: FlexProps) => (
  <Flex
    minH="12"
    position="sticky"
    zIndex={1}
    top="0"
    px="3"
    align="center"
    {...props}
  />
);

export const ColumnHeading = (props: HeadingProps) => (
  <Heading fontWeight="bold" fontSize="sm" lineHeight="1.25rem" {...props} />
);

export const ColumnButton = (props: ButtonProps) => (
  <Button
    variant="tertiary"
    size="sm"
    fontSize="xs"
    _hover={{ bg: 'gray.100' }}
    _active={{ bg: 'gray.200' }}
    _focus={{ boxShadow: 'none' }}
    _focusVisible={{ boxShadow: 'outline' }}
    {...props}
  />
);

export const ColumnIconButton = (props: IconButtonProps) => (
  <IconButton
    size="sm"
    fontSize="md"
    variant="tertiary"
    _focus={{ boxShadow: 'none' }}
    _focusVisible={{ boxShadow: 'outline' }}
    {...props}
  />
);
