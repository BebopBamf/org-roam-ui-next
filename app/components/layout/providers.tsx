'use client';

import { useState, useEffect, useMemo, useRef } from 'react';
import {
  ChakraProvider,
  extendTheme,
  withDefaultColorScheme,
} from '@chakra-ui/react';
import { Provider as ReduxProvider } from 'react-redux';
import { useAppDispatch, useAppSelector } from '@/hooks';
import { makeTheme } from '@/theme';
import { makeStore, type AppStore } from '@data/store';
import {
  connect,
  disconnect,
} from '@data/features/api-status/api-status-slice';
import { setFromLocalStorage } from '@data/features/color-scheme/color-scheme-slice';
import { setEmacsVariables } from '@data/features/emacs-variables/emacs-variables-slice';
import { setNotes } from '@data/features/notes/notes-slice';
import { WebSocket } from 'partysocket';

type ProvidersProps = {
  children: React.ReactNode;
};

function StoreProvider({ children }: ProvidersProps) {
  const storeRef = useRef<AppStore>();
  if (!storeRef.current) {
    storeRef.current = makeStore();
  }

  return <ReduxProvider store={storeRef.current}>{children}</ReduxProvider>;
}

function ApiProvider({ children }: ProvidersProps) {
  enum MessageType {
    GraphData = 'graphdata',
    Variables = 'variables',
    Theme = 'theme',
    Command = 'command',
  }

  const dispatch = useAppDispatch();

  const webSocketRef = useRef<WebSocket | null>(null);
  useEffect(() => {
    if (!webSocketRef.current) {
      webSocketRef.current = new WebSocket('ws://localhost:35903');
    }

    webSocketRef.current.addEventListener('open', () => {
      dispatch(connect());
      console.log('Connection with Emacs established.');
    });
    webSocketRef.current.addEventListener('message', (event) => {
      console.log('message', event.data);
      const message = JSON.parse(event.data);

      switch (message.type) {
        case MessageType.GraphData:
          console.log('graph data:', message.data);
          dispatch(setNotes(message.data));
          break;
        case MessageType.Variables:
          console.log('variables:', message.data);
          dispatch(setEmacsVariables(message.data));
          break;
        case MessageType.Theme:
          console.log('theme:', message.data);
          break;
        case MessageType.Command:
          console.log('command', message.data);
          break;
        default:
          console.error('Unknown message type', message.type);
      }
    });
    webSocketRef.current.addEventListener('close', () => {
      dispatch(disconnect());
      console.log('disconnected');
    });
  }, []);

  return children;
}

function ThemeProvider({ children }: ProvidersProps) {
  const dispatch = useAppDispatch();

  const colorScheme = useAppSelector((state) => state.colorScheme.colorScheme);
  const highlightColor = useAppSelector(
    (state) => state.colorScheme.highlightColor
  );

  const [isInitialized, setIsInitialized] = useState(false);

  useEffect(() => {
    if (isInitialized) {
      localStorage.setItem('colorScheme', JSON.stringify(colorScheme));
    }
  }, [colorScheme]);

  useEffect(() => {
    if (isInitialized) {
      localStorage.setItem('highlightColor', JSON.stringify(highlightColor));
    }
  }, [highlightColor]);

  useEffect(() => {
    dispatch(setFromLocalStorage());

    setIsInitialized(true);
  }, []);

  const themeColors = colorScheme[1];

  const appTheme = useMemo(
    () => makeTheme(themeColors, highlightColor),
    [highlightColor, JSON.stringify(colorScheme)]
  );

  const theme = extendTheme(
    appTheme,
    withDefaultColorScheme({ colorScheme: highlightColor.split('.')[0] })
  );

  return <ChakraProvider theme={theme}>{children}</ChakraProvider>;
}

export function Providers({ children }: ProvidersProps) {
  return (
    <StoreProvider>
      <ThemeProvider>
        <ApiProvider>{children}</ApiProvider>
      </ThemeProvider>
    </StoreProvider>
  );
}
