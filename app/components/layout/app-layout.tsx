'use client';

import { useState, useMemo } from 'react';
import { Box, Flex, HStack, Stack, Text } from '@chakra-ui/react';
import { Sidebar } from '@components/sidebar';
import { ColumnHeader, ColumnHeading, ColumnButton } from '@components/column';
import { FiFilter } from 'react-icons/fi';
import { useAppSelector } from '@/hooks';
import { Link } from '@chakra-ui/next-js';

type AppLayoutProps = {
  children: React.ReactNode;
};

export function TwoColumn({ children }: AppLayoutProps) {
  const [sidebarIsScrolled, setSidebarIsScrolled] = useState(false);

  const notes = useAppSelector((state) => state.notes.graphData.nodes);
  const sortedNotes = useMemo(() => {
    const _notes = notes?.slice();
    return _notes?.sort((a, b) => a.title.localeCompare(b.title));
  }, [notes]);

  return (
    <>
      <Box
        borderRightWidth="1px"
        width={{ md: '20rem', xl: '24rem' }}
        display={{ base: 'none', md: 'initial' }}
        overflowY="auto"
        onScroll={(event) =>
          setSidebarIsScrolled(event.currentTarget.scrollTop > 32)
        }
      >
        <ColumnHeader shadow={sidebarIsScrolled ? 'base' : 'none'}>
          <HStack justify="space-between" width="full">
            <HStack spacing="3">
              <ColumnHeading>Notes</ColumnHeading>
            </HStack>
            <ColumnButton leftIcon={<FiFilter />}>Filter</ColumnButton>
          </HStack>
        </ColumnHeader>
        <Stack spacing={{ base: '1px', lg: '1' }} px={{ lg: '3' }} py="3">
          {notes ? (
            sortedNotes.map((note) => (
              <Link
                href={`/notes/${note.id}`}
                key={note.id}
                _hover={{ textDecoration: 'none', bg: 'gray.100' }}
                _activeLink={{ bg: 'gray.700', color: 'white' }}
                borderRadius={{ lg: 'lg' }}
              >
                <Stack
                  spacing="1"
                  py={{ base: '3', lg: '2' }}
                  px={{ base: '3.5', lg: '3' }}
                  fontSize="sm"
                  lineHeight="1.25rem"
                >
                  <Text fontWeight="medium">{note.title}</Text>
                </Stack>
              </Link>
            ))
          ) : (
            <Text fontWeight="medium">No Notes Found!</Text>
          )}
        </Stack>
      </Box>
      {children}
    </>
  );
}

export function AppLayout({ children }: AppLayoutProps) {
  return (
    <Flex height="100vh">
      <Box
        height="full"
        width={{ md: '10rem', xl: '14rem' }}
        display={{ base: 'none', lg: 'initial' }}
        overflowY="auto"
        borderRightWidth="1px"
      >
        <Sidebar />
      </Box>
      {children}
    </Flex>
  );
}
