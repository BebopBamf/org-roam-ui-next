'use client';

import {
  type FlexProps,
  type As,
  Box,
  Flex,
  Stack,
  HStack,
  Spinner,
  Text,
  Icon,
} from '@chakra-ui/react';
import { Link, type LinkProps } from '@chakra-ui/next-js';
import { useAppSelector } from '@/hooks';
import { ColumnHeader, ColumnIconButton } from './column';

import {
  FiX,
  FiArrowUpRight,
  FiHome,
  FiBookOpen,
  FiCalendar,
  FiEye,
  FiSettings,
} from 'react-icons/fi';
import { FaCircleNodes } from 'react-icons/fa6';

interface NavLinkProps extends LinkProps {
  icon: As;
}

const NavLink = (props: NavLinkProps) => {
  const { icon, children, isExternal, ...linkProps } = props;

  return (
    <Link
      px="2"
      py="1.5"
      borderRadius="md"
      _hover={{ bg: 'gray.100' }}
      _activeLink={{ bg: 'gray.700', color: 'white' }}
      {...linkProps}
    >
      <HStack justify="space-between">
        <HStack as="a" spacing="3">
          <Icon as={icon} />
          <Text as="span" fontSize="sm" lineHeight="1.25rem">
            {children}
          </Text>
        </HStack>
        {isExternal && <Icon as={FiArrowUpRight} boxSize="4" />}
      </HStack>
    </Link>
  );
};

export function ConnectedTitle() {
  return (
    <HStack>
      <Text fontWeight="bold" fontSize="sm" lineHeight="1.25rem">
        Emacs Connected
      </Text>{' '}
      <Icon as={FiArrowUpRight} boxSize="4" />{' '}
    </HStack>
  );
}

export function DisconnectedTitle() {
  return (
    <HStack>
      <Text fontWeight="bold" fontSize="sm" lineHeight="1.25rem">
        Emacs Disconnected
      </Text>{' '}
      <Spinner boxSize="4" />{' '}
    </HStack>
  );
}

interface SidebarProps extends FlexProps {
  onClose?: () => void;
}

export function Sidebar(props: SidebarProps) {
  const { onClose } = props;
  const isConnected = useAppSelector(
    (state) => state.apiStatusReducer.connected
  );

  return (
    <Flex
      as="nav"
      h="full"
      direction="column"
      justify="space-between"
      {...props}
    >
      <Stack spacing="3">
        <ColumnHeader>
          <HStack spacing="3">
            <ColumnIconButton
              onClick={onClose}
              aria-label="Close navigation"
              icon={<FiX />}
              display={{ base: 'inline-flex', lg: 'none' }}
            />
            <Text fontWeight="bold" fontSize="sm" lineHeight="1.25rem">
              {isConnected ? <ConnectedTitle /> : <DisconnectedTitle />}
            </Text>
          </HStack>
        </ColumnHeader>

        <Stack px="3" spacing="6">
          <Stack spacing="1">
            {/* Org Roam Graph */}
            <NavLink href="/" icon={FiHome}>
              Home
            </NavLink>

            <NavLink href="/graph" icon={FaCircleNodes}>
              Graph
            </NavLink>

            {/* Org Roam Note Viewer */}
            <NavLink href="/notes" icon={FiBookOpen}>
              Read Notes
            </NavLink>

            {/* Org Roam Random Note Viewer */}
            <NavLink href="/random" icon={FiEye}>
              Random Note
            </NavLink>

            {/* Org Roam Dailies Viewer */}
            <NavLink href="/daily" icon={FiCalendar}>
              Read Daily
            </NavLink>
          </Stack>
        </Stack>
      </Stack>
      <Box>
        <Stack px="3" pb="3" spacing="6">
          <NavLink href="/settings" icon={FiSettings}>
            Settings
          </NavLink>
        </Stack>
      </Box>
    </Flex>
  );
}
